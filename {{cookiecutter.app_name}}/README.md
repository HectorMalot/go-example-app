{{cookiecutter.app_name}}

{{cookiecutter.project_short_description}}

## Getting started

This project requires Go to be installed. Go can be downloaded here: https://golang.org/dl/

Running it then should be as simple as:

```bash
make build
./bin/{{cookiecutter.app_name}}
```

## Testing

```bash
make test
```
------------

## Directory organization

    ├── Makefile           <- Makefile with commands like `make build` or `make test`
    ├── README.md          <- The top-level README for developers using this project.
    ├── cmd                <- All source files to build binaries from
    │   └── {{cookiecutter.app_name}}       <- Data from third party sources.
    ├── pkg                <- Source code for all libraries
    │   ├── cmd            <- Source code called by binaries from `/cmd`
    │   │   
    │   └── repository     <- Database adapter(s)
    │
    ├── db                <- Everything related to the database
    │   └── migrations    <- `dbmate` migrations
    │
    ├── support           <- Supporting scripts and files (e.g. for CI/CD)
    │   └── config.toml   <- Default configuration file
    │
    ├── bin               <- Compiled programs (not included in version control)
    │
    ├── tmp               <- Temporary files (not included in version control)
    │
    ├── .gitlab-cy.yml    <- CI definition for gitlab
    └── go.mod            <- Dependency management for go

