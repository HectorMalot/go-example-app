package cmd

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	rootCmd = &cobra.Command{
		Use:   "{{cookiecutter.app_name}}",
		Short: "Example app",
		Long:  `Example app`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
		},
	}
	verbose    bool
	configFile string
)

func init() {
	cobra.OnInitialize(loadConfig)
	viper.SetEnvPrefix("{{cookiecutter.app_name}}")

	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Enable verbose logging")
	rootCmd.PersistentFlags().StringVarP(&configFile, "config", "c", "support/config.toml", "Path to the config file")

	rootCmd.AddCommand(cmdExample)
}

// Execute registers alls commands and executes the relevant command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Error().Msg(err.Error())
		os.Exit(1)
	}
}

func loadConfig() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if verbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		log.Logger = log.With().Caller().Logger()

	}

	// Skip if no configFile was found
	if configFile == "" {
		log.Warn().Msg("no config file path provided")
		return
	}

	viper.SetConfigFile(configFile)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; we ignore the error and proceed with
			// defaults and environment variables
			log.Warn().Msg("config file not found")
		} else {
			// Config file was found but another error was produced
			log.Error().Msgf("fatal error while reading config file: %s", err)
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
