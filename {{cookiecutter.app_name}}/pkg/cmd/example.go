package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var (
	cmdExample = &cobra.Command{
		Use:   "example",
		Short: "Example output",
		Long:  `Example output`,
		Run:   example,
	}
)

func example(cmd *cobra.Command, args []string) {
	log.Info().Msg("Hello world!")
}
