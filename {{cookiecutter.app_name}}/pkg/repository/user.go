package repository

// User represents a user account
type User struct {
	ID           UserID  `db:"id"`
	Email        string  `db:"email"`
	PasswordHash string  `db:"password_hash"`
	CreatedAt    *string `db:"created_at"`
	UpdatedAt    *string `db:"updated_at"`
}

// UserID is the ID of the user
type UserID int
