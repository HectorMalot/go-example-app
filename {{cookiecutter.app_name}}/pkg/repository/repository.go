package repository

// Repository is the main interface to interact with the database
//
// This repository includes an example implementation to create and get users for postgresql
// note: it is not used in the example application
type Repository interface {
	CreateUser(User) (UserID, error)
	GetUser(UserID) (User, error)
}
