package repository

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // Using postgres driver

	"github.com/rs/zerolog"
)

// pgRepository adheres to the the Repository interface and is the interface for
// postgresql databases
type pgRepository struct {
	db     *sqlx.DB
	logger zerolog.Logger
}

// NewPgRepository initializes a new Repository with the provided sqlx database URL
func NewPgRepository(url string, logger zerolog.Logger) (Repository, error) {
	db, err := sqlx.Connect("postgres", url)
	if err != nil {
		return nil, err
	}
	return &pgRepository{db: db.Unsafe(), logger: logger}, nil
}

func (r *pgRepository) CreateUser(u User) (UserID, error) {
	var id UserID

	query := `
	INSERT INTO users
        (email, password_hash, created_at, updated_at)
    VALUES
		($1, $2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
	RETURNING id;
	`
	err := r.db.QueryRowx(query, u.Email, u.PasswordHash).Scan(&id)
	if err != nil {
		r.logger.Warn().Str("email", u.Email).Msg("Could not create user")
		return 0, err
	}
	return id, nil
}

func (r *pgRepository) GetUser(id UserID) (User, error) {
	u := User{}
	err := r.db.Get(&u, "SELECT * FROM users WHERE id=$1 LIMIT 1;", id)
	if err != nil {
		r.logger.Warn().Str("Method", "*pgRepository.GetUser()").Msg("Could not get user from database")
		return u, err
	}
	return u, nil

}
