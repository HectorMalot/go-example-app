package main

import "gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.app_name}}/pkg/cmd"

func main() {
	cmd.Execute()
}

