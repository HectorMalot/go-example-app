module gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.app_name}}

go 1.15

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.8.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
)
